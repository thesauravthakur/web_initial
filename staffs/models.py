from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()


class Staff(models.Model):
    name = models.CharField(max_length=250)
    email = models.EmailField(max_length=254, unique=True)
    address = models.CharField(max_length=350)
    phone = models.IntegerField()
    department = models.CharField(max_length=250)
    image = models.ImageField(upload_to='pictures', null=True, blank=True)
    description = models.TextField(max_length=500)
    document = models.FileField(blank=False, null=False)

    def __str__(self):
        return self.name
