from django.shortcuts import render
from rest_framework.parsers import MultiPartParser, FormParser
from .serializers import StaffSerializer
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework import status
from .models import Staff
from rest_framework.response import Response


class StaffView(viewsets.ModelViewSet):
    parser_classes = (MultiPartParser, FormParser)
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer

