from django.urls import path

from .views import StaffView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'', StaffView)
urlpatterns = router.urls
